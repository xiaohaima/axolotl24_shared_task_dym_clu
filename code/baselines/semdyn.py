import bertcloud as bc
import sensecluster as sc
import visualization as vs
import utils


def gen_cloud(keyword, corpus, cache_path=None):
    if cache_path is not None:
        cloud = utils.load_from_disk(cache_path+'cloud.pkl')
        static_embeds = utils.load_from_disk(cache_path+'static_embeds.pkl')
        if cloud is not None and static_embeds is not None:
            return cloud, static_embeds
    prbert_model = 'bert-base-multilingual-cased'
    cloud, stat_emb = bc.bert_cloud(keyword, corpus, prbert_model)
    static_embeds = utils.init_static_embedding()
    utils.update_static_embeds(static_embeds, stat_emb)
    if cache_path is not None:
        utils.save_to_disk(cache_path+'cloud.pkl', cloud)
        utils.save_to_disk(cache_path+'static_embeds.pkl', static_embeds)
        utils.save_sendata_as_txt(cache_path+'corpus.txt', corpus)

    return cloud, static_embeds


if __name__ == '__main__':
    keyword = 'mouse'
    corpus_path1 ='./data/words/en/mouse/sendata/sendata1.txt'
    corpus_path2 ='./data/words/en/mouse/sendata/sendata2.txt'
    corpus1 = utils.load_text_corpus(corpus_path1)
    corpus2 = utils.load_text_corpus(corpus_path2)
    # cloud1, static_embeds1 = gen_cloud(keyword, corpus1, cache_path='./.cache/words/mouse/1/')
    # cloud2, static_embeds2 = gen_cloud(keyword, corpus2, cache_path='./.cache/words/mouse/2/')
    cloud1, static_embeds1 = gen_cloud(keyword, corpus1)
    cloud2, static_embeds2 = gen_cloud(keyword, corpus2)
    static_embeds = utils.merge_static_embeds(static_embeds1, static_embeds2)
    t1=0.78
    t2=0.62
    # sense_clu1, lf_semantic1 = \
    #         sc.cloud_cluster(cloud1, static_embeds, keyword, 'en', k=14, rt=t1, sct=t2,
    #                          cache_time='1')
    # sense_clu2, lf_semantic2 = \
    #         sc.cloud_cluster(cloud2, static_embeds, keyword, 'en', k=14, rt=t1, sct=t2,
    #                          cache_time='2')
    sense_clu1, lf_semantic1 = \
            sc.cloud_cluster(cloud1, static_embeds, keyword, 'en', k=14, rt=t1, sct=t2)
    sense_clu2, lf_semantic2 = \
            sc.cloud_cluster(cloud2, static_embeds, keyword, 'en', k=14, rt=t1, sct=t2)
    sim_matrix = sc.sim_matrix(sense_clu1, sense_clu2, static_embeds)
    change_matrix = sim_matrix > t2
    # vs.plt_sense_dynamic_graph(sense_clu1, cloud1, sense_clu2, cloud2, change_matrix, k=8)
    print("sims ", sim_matrix)
    print("xiaohaima")

