

def assessment_def_block(
        target_word,
        sentences,
        luanguage,
        prompt_placeholder="",
        response_placeholder="",
):
    # instructions0 = ["Describe the sense with respect to the document of a target word",
    #                  "then give the definition of the target word in a way that facilitates understanding and lexicographical research."]
    # instructions0 = ["The following document contains "+len(sentences)+" sentences,",
    #                  f'which describe the same sense of the target word "{target_word}".',
    #                  "The sentences are:"]

    instructions = [f'Give the short definition of the sense of the target {luanguage} word "{target_word}" ' +
                    "which is described in the following sentences:"]
    sentences_list = [f'{i+1}. {sen}' for i, sen in enumerate(sentences)]
    requirements = ["Requirements:",
                    "The given definition should be in a way "+
                    "that facilitates understanding and lexicographical research."]
    output_prompt = ["Output: {{gen 'definition' pattern='str'}}"]
    prompt_pieces = [prompt_placeholder] + instructions + sentences_list + requirements +\
                    [response_placeholder] + output_prompt
    return "\n".join(prompt_pieces)


def main():
    u_prompt = "### Human:"
    a_prompt = "### Assistant:"

    prompt_placeholder = u_prompt
    response_placeholder = a_prompt

    print("---------------------------------------")

    target_word = 'head_w'
    luanguage = 'Russian'
    sentences = ["I like to eat fish the restaurant .",
                 "I like to eat snails at the restaurant .",
                 "I like to eat fish and snails at the restaurant ."]
    prompt = assessment_def_block(
         target_word,
         sentences,
         luanguage,
         response_placeholder=response_placeholder,
         prompt_placeholder=prompt_placeholder)
    print(prompt)


if __name__ == "__main__":
    main()


